<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');
Route::resource('/home', 'Api\HomeController');

Route::group(['middleware'=>['jwt.auth']],function(){
    Route::resource('/user','Api\UserController');
});
Route::post('/login','Api\AuthController@login');
Route::get('/login/admin','Api\AuthController@getLoginUser');
Route::resource('event', 'Api\EventController',['only' => ['index', 'show']]);
Route::resource('category', 'Api\CategoryController',['only' => ['index']]);
Route::resource('comment', 'Api\CommentController',['only' => ['store']]);
Route::post('category/{id}/favorite', 'Api\CategoryController@favorite');
Route::post('category/{id}/unfavorite', 'Api\CategoryController@unfavorite');
Route::post('event/{id}/like', 'Api\EventController@like');
Route::post('event/{id}/unlike', 'Api\EventController@unlike');
