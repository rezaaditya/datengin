<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/login', 'HomeController@index');

Auth::routes();
Route::group(['namespace' => 'Admin', 'prefix'=>'admin'], function(){
  Route::get('/dashboard', 'DashboardController');
  Route::resource('/category','CategoryController');
  Route::get('{id}/delete','CategoryController@delete');
  Route::resource('/event','EventController');
  Route::get('{id}/delete','EventController@delete');

});


Route::get('/logout', function(){
  Auth::logout();
});
