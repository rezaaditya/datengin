<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable=['title','description','price','date','location','speaker','img_url','user_id','category_id'];
    public function user()
    {
    	return $this->belongsTo('App\User');
    }
    public function category()
    {
    	return $this->belongsTo('App\Category');
    }

}
