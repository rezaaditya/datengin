<!DOCTYPE html>
<html lang="en">

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Datengin! | </title>

    <!-- Bootstrap -->
    <link href="{{asset('gentelella/vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('gentelella/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Animate.css -->
    <link href="https://colorlib.com/polygon/gentelella/css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{asset('gentelella/build/css/custom.min.css')}}" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>
      <div class="register_wrapper">

        <div id="register" class="animate form registration_form">
          <section class="register_content">
            {!!Form::open(['action' => ['Auth\RegisterController@register']]) !!}
              <h1>Create Account</h1>

              <div class="form-group">
                {!! Form::text('username',null,["class" =>"form-control", "placeholder"=>"Username"]) !!}
                <!-- <input type="text" class="form-control" placeholder="Username" required="" /> -->
                <br>
              </div>

              <div class="form-group">
                {!! Form::text('email',null,["class" =>"form-control", "placeholder"=>"Email"]) !!}
                <!-- <input type="email" class="form-control" placeholder="Email" required="" /> -->
                <br>
              </div>

              <div class="form-group">
                {!! Form::text('password',null,["class" =>"form-control", "placeholder"=>"Password"])!!}
                <!-- <input type="password" class="form-control" placeholder="Password" required="" /> -->
                <br>
              </div>

              <div>
                <a class="btn btn-default submit" href="index.html">Submit</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Already a member ?
                  <a href="#signin" class="to_register"> Log in </a>
                </p>

                <div class="clearfix"></div>
                <br />
                <div>
                  <h1><i class="fa fa-paw"></i> Datengin !</h1>
                  <p>©2016 All Rights Reserved. Datengin! is a Bootstrap 3 template. Privacy and Terms</p>
                </div>
              </div>
            {!! Form::close()!!}
          </section>
        </div>
      </div>
  
