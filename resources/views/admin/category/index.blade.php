@extends('admin.layouts.blank')

@section('title','Index')

@section('content')


<h1>Category List</h1>
        <p class="lead" ></p>
        <div class="table-responsive">
            <table class="table table-bordered">
              <tr>
                <th>No.</th>
                <th>Name</th>
                <th>Options</th>


              </tr>

        @foreach($data as $category)

        <tr>
          <td>
            {{ $loop->iteration}}
          </td>
        <td>{{$category->name}}</td>

      </br>
        <td>{{$category->options}}
                <a href="{{action('Admin\CategoryController@show',$category->id )}}" class="btn btn-info"> View Category</a>
                <a href="{{action('Admin\CategoryController@edit', $category->id)}}" class="btn btn-primary"> Edit Category</a>
                <a href="{{action('Admin\CategoryController@delete', $category->id)}}" class="btn btn-danger"> Delete Category</a>
              </td>
      </tr>
        <hr>
      @endforeach
    </table>
  </div>



@endsection
