@extends('admin.layouts.blank')
@section('title','Edit')
@section('content')
  {!! Form::model($data,['method'=>'put','action'=>['Admin\EventController@update',$data->id ]])!!}
  <div class="form-group">
          {!! Form::label('title', 'title:', ['class' => 'control-label']) !!}
          {!! Form::text('title', null, ['class' => 'form-control']) !!}
  </div>

  <div class="form-group">
    {!! Form::label('description', 'description:', ['class' => 'control-label']) !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        </div>

  <div class="form-group">
          {!! Form::label('price', 'price:', ['class' => 'control-label']) !!}
          {!! Form::text('price', null, ['class' => 'form-control']) !!}
  </div>

  <div class="container">
      <div class="row">
          <div class='col-sm-6'>
              <div class="form-group">
                {!! Form::label('date', 'date:', ['class' => 'control-label']) !!}
                  <div class='input-group date' id='datetimepicker1'>
                    <input name="date" type='datetime' class="form-control" data-date-format="YYYY-MM-DD hh:mm:ss"/>
                      <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                  </div>
              </div>
          </div>

      </div>
  </div>

  <div class="form-group">
          {!! Form::label('location', 'location:', ['class' => 'control-label']) !!}
          {!! Form::text('location', null, ['class' => 'form-control']) !!}
  </div>

  <div class="form-group">
          {!! Form::label('speaker', 'speaker:', ['class' => 'control-label']) !!}
          {!! Form::text('speaker', null, ['class' => 'form-control']) !!}
  </div>

  <div class="form-group">
          {!! Form::label('img', 'images:', ['class' => 'control-label']) !!}
          {!! Form::file('img', null, ['class' => 'form-control']) !!}
  </div><br>


  {!! Form::submit('Update Event', ['class' => 'btn btn-primary']) !!}
  {!! Form::close()!!}

  @endsection
